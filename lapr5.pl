:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/json)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(odbc)).

%--------------------------------------------------------------------------------------------------------------------------------
% Ligação base de dados

%Exemplo de uso
use_odbc:- open_wordnet,
( poidb(X,_,_,T), write([X,'-',T]), fail;true ),
disconnect_database().

%Predicado de ligação
%modificar quando tivermos os dados de acesso
open_wordnet :-
    odbc_connect('NomeDaLigacaoOdbcDoComputador', _,
                 [ user(user),
                   password(pass),
                   alias(mysqlDB),
                   open(once)
                 ]).

%Chamadas à DB
%alterar nome dos campos em conformidade com a DB
poidb(ID, HoraAbertura, HoraFecho, TempoEstimadoVisita):- odbc_query(mysqlDB, 'SELECT id, horaabertura, horafecho, tempoestimadovisita FROM poi', row(ID, HoraAbertura, HoraFecho, TempoEstimadoVisita),
                 			[ types([integer,default,default,integer]) ]).

caminhodb(ID, PInicio, PFim, Distancia):- odbc_query(mysqlDB, 'SELECT id, pinicio, pfim, distancia FROM caminho', row(ID, PInicio, PFim, Distancia),
                                       			[ types([integer,integer,integer,float]) ]).

%Predicado de fecho de ligação
disconnect_database() :- odbc_disconnect(mysqlDB).

%--------------------------------------------------------------------------------------------------------------------------------
% Pedidos HTTP API

% Relação entre pedidos HTTP e predicados que os processam
:- http_handler('/sugerir_percurso', sugerir_percurso, []).
:- http_handler('/planear_visita', planear_visita, []).

% Criação de servidor HTTP no porto 'Port'
start_server(Port):-http_server(http_dispatch, [port(Port)]).

% Eliminação de servidor HTTP no porto 'Port'
stop_server(Port):-http_stop_server(Port, []).

% MÉTODO GET Exemplo: Tratamento de 'http://localhost:Port/sugerir_percurso?poi_inicial=1&poi=1&poi=3'
% ou http_client:http_get('http://localhost:Port/sugerir_percurso?poi_inicial=1&poi=1&poi=3',X,[]).

% MÉTODO POST Exemplo
% http_client:http_post('http://localhost:5000/register_user', form_data([poi_inicial=1, poi=1, poi=2]), Reply, []).
% Conforme a quantidade do campo poi recebido é formulada a lista automáticamente de pois a visitar

sugerir_percurso(Request) :-
    http_parameters(Request,
                    [
                      poi_inicial(PI, [integer]),
                      poi(L, [list(integer)])
                    ]),

    %lógica negócio (chamar os predicados necessários nesta fase)

    %json([campo=valor,campo=valor])
    prolog_to_json(json([poiInicial=PI,listaPois=L]), JSONOut),
    reply_json(JSONOut).


%--------------------------------------------------------------------------------------------------------------------------------
% Lógica de negócio
% Ponto 1 de 2
% Otimizar sequência de visita aos POIs

% Critérios:
% 1 - Duracao total da visita; 2 - quilometragem,
% 3 - inclinação; 4 - tempo de percurso pé/carro
% 5 - outros critérios, eventualmente.

% Hipotese de solucão. Determinar a melhor sequencia de POIs
% dado um valor comulativo que é gerado à medida que é calculado o
% valor resultante de cada critério para cada sequência de POIs.

% Pesos para os critérios
% Perfil 1 - Critérios em equidade
% Duracao total da visita - 25 %; quilometragem - 25%
% inclinação - 25 %; tempo de percurso - 25 %

% Perfil 2 - ...




%-------------------------------------------------------------------------------------------------------------------------------

		
%diferença de tempo em minutos
%usado para ver o tempo máximo que a traversia deve ter
minDif(time(H1, M1, _), time(H2, M2, _), D):-D is ((H2 - H1)* 60) + ((M2 - M1) mod 60).

%return de lista (ID de caminho, ID de ponto origem, ID de ponto chegada,Distancia) entre dois pontos fornecidos
%false se caminho nas condições fornecidas for impossivel.
path(Or,Dest,Incl,List):- path(Or,Dest,[Or],[],Incl,List).
path(Dest,Dest,_,_,_,[]).
path(Or,Dest,PList,PathID,Incl,[(ID,Or,Yonder)|T]):- caminho(ID,Or,Yonder,Inc),Inc =< Incl,\+member((ID,Or,Yonder),PathID), member_path(1,Yonder,PList),path(Yonder,Dest,[Yonder|PList],[(ID,Or,Yonder)|PathID],Incl,T).

%member que verifica se foi usado um destino mais do que x vezes, caminho pouco recomendável
member_path(N,Dest,List) :- count(List,Dest,Ocurrences), N >= Ocurrences.

count([],_,0).
count([X|T],X,Y):- count(T,X,Z), Y is 1+Z.
count([X1|T],X,Z):- X1\=X,count(T,X,Z).

%recebe [(ID,OR,DEST),(ID,OR,DEST)]
%quer-se que retorne uma lista de ID's de Pontos que o caminho percorreu
pointList([],[]).
pointList([(_,_,DEST)|L], [DEST|List]):- pointList(L,List).

%Recebe Origem,Lista POIs desejados,Lista POIs desejados,Inclinação máxima, Caminho que inclui todo(return)
%verifica se o caminho entre a origem designada e os pontos desejados, se algum englobar todos, IS NICE
poiPath(_,[],_,_,[],_,_).
poiPath(Or,[X|PointList],AuxPointList,Incl,[Optimum|ValidPaths],StartingMinutes,Transporte):- 
		findViablePaths(Or,X,AuxPointList,Incl,Optimum,StartingMinutes,Transporte),!,poiPath(Or,PointList,AuxPointList,Incl,ValidPaths,StartingMinutes,Transporte).
poiPath(Or,[_|PointList],AuxPointList,Incl,ValidPaths,StartingMinutes,Transporte):- poiPath(Or,PointList,AuxPointList,Incl,ValidPaths,StartingMinutes,Transporte).
	
%encontrar caminho válido dado a inclinação máxima e pontos desejados, e caminho de retorno
%listas vazias são retornados se não for possivel caminho
findPath(Origin, ListPOIs, MaxIncl,ForwardPath,ReturnPath,StartingMinutes,Transporte, MaxKilometers, TotalKilometers,ElapsedTime):- shortest_valid_path(Origin,ListPOIs,MaxIncl,ForwardPath,DestinationPoint,StartingMinutes,Transporte),length(ForwardPath,L), L > 0,path(DestinationPoint,Origin,MaxIncl,ReturnPath),
	path_distance(ForwardPath,FDist), TotalKilometers is FDist * 111.113, TotalKilometers =< MaxKilometers, sumOfElapsedTime(ForwardPath,ElapsedTime,Transporte).

%find all the poi path, aglutinar em lista (distance, destination point,[path]), sort, return first
shortest_valid_path(Origin, ListPOIs,MaxIncl,Path,DestinationPoint,StartingMinutes,Transporte):- 
	poiPath(Origin,ListPOIs,ListPOIs,MaxIncl,OptimumList,StartingMinutes,Transporte), sort(0,@<, OptimumList,[(_,DestinationPoint,Path)|_]).
	
%return soma de distancia de caminhos listados
path_distance([],0).
path_distance([(_,Origem,Destino)|List],Total):- path_distance(List,D1), poi(Origem,_,_,_, XO,YO), poi(Destino,_,_,_,XD,YD), 
		Distance is sqrt((XD-XO)*(XD-XO)+(YD-YO)*(YD-YO)),Total is Distance + D1. 

%Lista recebida é [(ID,OR,DEST),(ID,OR,DEST)]
checkPOITimeTables([],_,_).
checkPOITimeTables([(ID,_,DEST)|L], StartingMinutes,Transporte):- time_forward_path(ID,StartingMinutes,NewTime,Transporte),checkPOIAvailability(DEST,NewTime),checkPOITimeTables(L,NewTime,Transporte).

%Euclidian 
%For the love of all that is holy, let the velocity be realistic, a float in terms of kilometers per minute
% R is floor(sqrt((XD-XO)*(XD-XO)+(YD-YO)*(YD-YO)))
%recebe ID de origem e destino, vai buscar coordenadas de ambos, calcula distancia, aumenta tempo atual pelo tempo que demorará em média (velocidade 100 m min) + permanencia no POI
time_forward_path(PathID,CurrentTime,ForwardedTime,Transporte):- caminho(PathID,Origem,Destino,_),
	poi(Origem,_,_,_, XO,YO), poi(Destino,_,_,Perm,XD,YD), Distance is sqrt((XD-XO)*(XD-XO)+(YD-YO)*(YD-YO)),
	Kilometers is Distance * 111.113, transporte(Transporte,Velocidade),ElapsedTime is Kilometers / Velocidade, ForwardedTime is CurrentTime + ElapsedTime + Perm.

%vai buscar minutos totais da hora de abertura e fecho do ponto
%compara se tempo de acesso está entre eles.
checkPOIAvailability(ID,AcessTime):- poi(ID,OpenTime,ClosingTime,_,_,_), to_minutes(OpenTime,O), O =< AcessTime,to_minutes(ClosingTime,C),AcessTime =< C.	
	
%CONVERSÃO time(H,M,S) to min
to_minutes(time(H,M,_),Result):- Result is M + (H * 60).	
	
%[ [(ID,OR,DEST),(ID,OR,DEST)], [(ID,OR,DEST),(ID,OR,DEST)] ]
%retorne caminho que passe em todos os pois desejados, não há condição de paragem, must be false
%findValidPath(_,_,[],_,_,_).
%findValidPath(_,_,[Path|_],Path,StartingMinutes,Transporte):- checkPOITimeTables(Path,StartingMinutes,Transporte).
%findValidPath(Origem,PointList,[_|L],ValidPath,StartingMinutes,Transporte):- findValidPath(Origem,PointList,L,ValidPath,StartingMinutes,Transporte).

%adicionar origem na lista de pontos percorridos, resolvendo lacuna de predicados anteriores
addOrigin(Origem,PointPassageList,[Origem|PointPassageList]).

%return de lista de listas, sendo que cada lista interna contem (ID de caminho, ID de ponto origem, ID de ponto chegada,Distancia)
%false se caminho nas condições fornecidas for impossivel
findViablePaths(PontoPartida, IDChegada, POIList, Inclinacao, Optimum,StartingMinutes,Transporte):- 	
	findall(
	(Distance,IDChegada,List),
	(path(PontoPartida,IDChegada,Inclinacao,List), pointList(List,PointPassageList),addOrigin(PontoPartida,PointPassageList,ResultList),checkAllPOIPassage(POIList,ResultList),
	checkPOITimeTables(List,StartingMinutes,Transporte), path_distance(List,Distance)),
	ListaCaminhos
	), sort(0,@<, ListaCaminhos,[Optimum|_]).
	
%condicao de paragem
checkAllPOIPassage(ListaPOIs, ListaPercorrida):- checkPOIPassage(ListaPOIs, ListaPercorrida).
%feito para que lista seja uma lista de IDs
checkPOIPassage([],_).
checkPOIPassage([H|L],ListaPercorrida):- member(H,ListaPercorrida), !, checkPOIPassage(L,ListaPercorrida).
			
%obtenção de lista com todos os id's conhecidos, assumindo que todos os ID's estão devidamente entrosados na rede com quantidade devida de caminhos.
allIDList(ReturnList):- 
	findall(
	(ID),
	(poi(ID,_,_,_,_,_)),
	ReturnList
	).
	
%predicado especifico para caminhos de retorno, menor distancia
%destino até origem
%DestinoFinal, Origem, Inclinacao, 	
%return_Path(DestinoFinal, Origem, Inclinacao, RPath):-
%	findall(
%	(Distance, ReturnPath),
%	(path(DestinoFinal,Origem,Inclinacao,ReturnPath), 
%		path_distance(ReturnPath, Distance),
%		Distance > 0),
%	AggregateList), sort(0,@<, AggregateList,[(_,RPath)|_]).
	
%return soma de minutos gasto no trajeto
sumOfElapsedTime([],0,_).
sumOfElapsedTime([(_,Origem,Dest)|List],Total,Transporte):- sumOfElapsedTime(List,Elapsed,Transporte),poi(Origem,_,_,_,XO,YO),poi(Dest,_,_,Perm,XD,YD),
	Distance is sqrt((XD-XO)*(XD-XO)+(YD-YO)*(YD-YO)),Kilometers is Distance * 111.113,transporte(Transporte,Velocidade),ElapsedTime is Kilometers / Velocidade,TotalTime is ElapsedTime + Perm, Total is Elapsed + TotalTime. 	
	
%converte horas em minutos para comparacao
% para duração de x horas, admite-se duração minima de x horas e máxima de x horas + meia hora de tolerância
% vê se a transição para um dado ponto é execuivel, incluindo retorno, no período de tempo supracitado
% random permutation é executado na lista, para que caminho não seja sempre o mesmo, dado que lista é recolhida de maneira constante
findRingPathWithMaxHours(Origem, MaxHours, HoraInicial,Inclinacao,Path, ReturnPath,Transporte,MaxKilometers, Distance, Time):-
	allIDList(IDList),random_permutation(IDList, SortedList),to_minutes(HoraInicial, MinutosIniciais), MaxMinutes is MaxHours * 60, Tolerance is MaxMinutes + 30,
	findViableTimedLoopPath(Origem, MaxMinutes, Tolerance,MinutosIniciais, Inclinacao,SortedList, Path, ReturnPath,Transporte,MaxKilometers, Distance, FTime),
	sumOfElapsedTime(ReturnPath,RTime,Transporte), Time is FTime + RTime.
	
%false se nao for possivel
findViableTimedLoopPath(Origem, MaxDurationMinutes, ToleranceMinutes,InitialMinutes, Inclinacao,[(ID)|_], Path,ReturnPath,Transporte,MaxKilometers,TotalKilometers, FTime):- findPath(Origem,[ID],Inclinacao,Path,ReturnPath,InitialMinutes,Transporte,360,FDist,FTime), 
	sumOfElapsedTime(ReturnPath,R,Transporte), R > 0,Sum is R + FTime,MaxDurationMinutes =< Sum, Sum =< ToleranceMinutes, 
	path_distance(ReturnPath,ReturnDistance), 
	ReturnKilometers is ReturnDistance * 111.113, TotalKilometers is ReturnKilometers + FDist,TotalKilometers =< MaxKilometers. 
findViableTimedLoopPath(Origem, MaxDurationMinutes, ToleranceMinutes,InitialMinutes, Inclinacao,[_|List], Path,ReturnPath,Transporte,MaxKilometers,TotalKilometers, FTime):- findViableTimedLoopPath(Origem, MaxDurationMinutes, ToleranceMinutes,InitialMinutes, Inclinacao,List, Path,ReturnPath,Transporte,MaxKilometers,TotalKilometers,FTime).
	