:-	initialization go.

go :- 
	open('Parameters_1.txt',read,Str),
    read(Str,Origin),
	read(Str,POIList),
	read(Str,Inclination),
	read(Str,StartingMinutes),
	read(Str,Transporte),
	read(Str, MaxKilometers),
    close(Str),
	consult(lapr5),
	consult(knowledge),
	findPath(Origin, POIList, Inclination,ForwardPath,ReturnPath,StartingMinutes,Transporte,MaxKilometers,Distance,Time),
    open('Results_1.txt',write,Out),
	write(Out,ForwardPath),
	write(Out,'\n'),
	write(Out,ReturnPath),
	write(Out,'\n'),
	write(Out,Distance),
	write(Out,'\n'),
	write(Out,Time),
	close(Out),
	halt(0).
	
go :-
	halt(0).
	
	