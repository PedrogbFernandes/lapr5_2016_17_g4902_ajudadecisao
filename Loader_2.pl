:-	initialization go.

go :- 
	open('Parameters_2.txt',read,Str2),
    read(Str2,Origem),
	read(Str2,MaxHours),
	read(Str2,HoraInicial),
	read(Str2,Inclinacao),
	read(Str2,Transporte),
	read(Str2,MaxKilometers),
    close(Str2),
	consult(lapr5),
	consult(knowledge),
	findRingPathWithMaxHours(Origem, MaxHours, HoraInicial,Inclinacao,Path, ReturnPath,Transporte,MaxKilometers,Distance,Time),
    open('Results_2.txt',write,Out2),
	write(Out2,Path),
	write(Out2,'\n'),
	write(Out2,ReturnPath),
	write(Out2,'\n'),
	write(Out2,Distance),
	write(Out2,'\n'),
	write(Out2,Time),
	close(Out2),
	halt(0).
	
go :- 
	halt(0).

	