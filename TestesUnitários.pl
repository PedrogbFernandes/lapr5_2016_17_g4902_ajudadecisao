/* CSE 341 - Unit testing in SWI Prolog
   Here are some simple examples of doing unit tests.  For additional
   details see http://www.swi-prolog.org/pldoc/package/plunit.html
   To use, just read in this unit testing file. */

/* We'll use the basics lecture notes for examples.  Note the syntax
   here for a directive for consulting another file. */
:- consult(lapr5).
:- consult(knowledge_alternate).

:- begin_tests(lapr5).

% Testa método conversor de tempo para minutos
test(to_minutes):- to_minutes(time(0,0,_),Result), Result is 0.
test(to_minutes):- to_minutes(time(1,31,_),Result), Result is 91.
test(to_minutes):- to_minutes(time(9,45,_),Result), Result is 585.
test(to_minutes):- to_minutes(time(18,20,_),Result), Result is 1100.

% Testa método diferença entre tempos
test(minDif):- minDif(time(16, 00, _), time(16, 00, _), D), D is 0.
test(minDif):- minDif(time(16, 00, _), time(17, 00, _), D), D is 60.
test(minDif):- minDif(time(18,50, _),time(17,30,_),D), D is -20.

%Testa extração de ID's destino de cada caminho percorrido
test(pointList):- pointList([(1,101,102),(2,102,103),(3,103,104)],List), not(List \= [102,103,104]).
test(pointList):- pointList([(1,101,102),(2,102,103),(3,103,104),(4,104,105),(5,105,106),(6,106,107)],List), not(List \= [102,103,104,105,106,107]).

%Teste de adição de membro na  cabeça de lista
%addOrigin(Origem,PointPassageList,[Origem|PointPassageList])
test(addOrigin):- addOrigin(100,[101,102,103],List), not(List \= [100,101,102,103]).

%Teste de forward time aquando da transposição de um caminho
test(time_forward_path):- time_forward_path(12,500,Time,pe), Time is 667.9533252905879. 
test(time_forward_path):- time_forward_path(12,500,Time,carro), Time is 627.9922208817646.

%Teste de predicado path entre dois pontos
test(path):- path(2,9,50,P), not(P \= [ (0, 2, 10), (1, 10, 2), (2, 2, 47), (7, 47, 9)]). 
test(path):- path(2,61,50,P), not(P \= [ (0, 2, 10), (1, 10, 2), (4, 2, 63), (19, 63, 61)] ).

%Teste de procura de poiPath
test(poiPath):- poiPath(2,[9,10,47],[9,10,47],50,Optimum,500, pe), DestinationPoint is 9, not(Path \= [ (0.06453478508335263, 9, [ (0, 2, 10), (15, 10, 47), 
	(7, 47, 9)]), (0.08694496568238044, 10, [ (2, 2, 47), (7, 47, 9), (11, 9, 10)]), 
	(0.08367692700322843, 47, [ (0, 2, 10), (10, 10, 9), (6, 9, 47)])]).

%teste de procura de caminho válido para todos os critérios
%findPath(Origin, ListPOIs, MaxIncl,ForwardPath,ReturnPath,StartingMinutes,Transporte, MaxKilom)
test(findPath):- findPath(2,[9,10,47,61,62],60, F,R, 600, carro, 20, FDist, Time), not(F \=  [ (17, 2, 61), (23, 61, 62), (20, 62, 2), (0, 2, 10), (15, 10, 47), (7, 47, 9)]),
	not(R \= [ (6, 9, 47), (3, 47, 2)]).
test(findPath):- not(findPath(2,[9,10,47,61,62],0, F,R, 600, carro, 20, FDist, Time)).

%teste de procura de caminho mais curto que passa por vários pontos pre-determinados
%shortest_valid_path(Origin, ListPOIs,MaxIncl,Path,DestinationPoint,StartingMinutes,Transporte):- 
test(shortest_valid_path):- shortest_valid_path(2,[9,10,47],60,P,D,600,pe), not(Path \= [ (0, 2, 10), (15, 10, 47), (7, 47, 9)]), DestinationPoint is 9.
test(shortest_valid_path):- shortest_valid_path(2,[9,10,47,61],60,P,D,600,carro), not(Path \= [ (17, 2, 61), (16, 61, 2), (0, 2, 10), (15, 10, 47), (7, 47, 9)]),DestinationPoint is 9.

%teste de path distance
test(path_distance):- path_distance([ (0, 2, 10), (1, 10, 2), (4, 2, 63), (19, 63, 61)] , D), D is 0.1096536682167806.
test(path_distance):- path_distance( [ (0, 2, 10), (1, 10, 2), (2, 2, 47), (7, 47, 9)],F), F is 0.12302205728278467.

%Teste de verificação de horário de vários POI's dado o tempo inicial de traversia em minutos
test(checkPOITimeTables):- checkPOITimeTables([ (0, 2, 10), (1, 10, 2), (4, 2, 63), (19, 63, 61)],500,pe).

%Teste de verificação de disponibilidade de POI numa dada altura dada em minutos
test(checkPOIAvailability):- checkPOIAvailability(9,500).

%Teste de procura de todos os caminhos possiveis entre dois pontos
%findViablePaths(PontoPartida, IDChegada, Inclinacao, ListaCaminhos):
test(findViablePaths):- findViablePaths(2,9,[10,47],60,Optimum,600,pe), not(Optimum \= (0.06453478508335263, 9, [ (0, 2, 10), (15, 10, 47), (7, 47, 9)])).
test(findViablePaths):- not(findViablePaths(2,9,[10,47],0,Optimum,600,pe)).
test(findViablePaths):- findViablePaths(2,9,[10,47,61],60,Optimum,600,pe), not(Optimum \=  (0.07398960332592404, 9, [ (17, 2, 61), (16, 61, 2), (0, 2, 10), (15, 10, 47), (7, 47, 9)])).

%teste de soma de tempo decorrrido no caminho dado, dado um dado meio de transporte
%sumOfElapsedTime([],0,_)
test(sumOfElapsedTime):- sumOfElapsedTime([ (0, 2, 10), (1, 10, 2), (4, 2, 63), (19, 63, 61)], F,pe), F is 488.6789607314229.
test(sumOfElapsedTime):- sumOfElapsedTime([ (17, 2, 61), (16, 61, 2), (0, 2, 10), (15, 10, 47), (7, 47, 9)], F,carro), F is 422.40402264784467.

%Teste de predicado principal de visita com máximo de horas
%findRingPathWithMaxHours(Origem, MaxHours, HoraInicial,Inclinacao,Path, ReturnPath,Transporte,MaxKilometers)
test(findRingPathWithMaxHours):- findRingPathWithMaxHours(2,4,time(9,0,0),50,P,R,pe,20,Distance,Time), not(P \= [ (0, 2, 10)]), not(R \= [ (1, 10, 2)]), Distance is 7.27237466971736.

%Teste de predicado auxiliar de visita com máximo de horas
%findViableTimedLoopPath(Origem, MaxDurationMinutes, ToleranceMinutes,InitialMinutes, Inclinacao,[(ID)|_], Path,ReturnPath,Transporte,MaxKilometers)
teste(findViableTimedLoopPath):- findViableTimedLoopPath(2, 600, 1000, 500, 50, [9,10], P,R, pe, 20,Distance, Time), not(P \= [ (2, 2, 47), (7, 47, 9)]), not(R \= [ (6, 9, 47), (3, 47, 2)]), Distance is 12.793950362289387, Time is 367.9395036228939.
teste(findViableTimedLoopPath):- not(findViableTimedLoopPath(2, 600, 1000, 500, 50, [9,10], P,R, pe, 10,Distance, Time)).
teste(findViableTimedLoopPath):- findViableTimedLoopPath(2, 600, 1000, 500, 50, [9,10,47,60], P,R, pe, 20,Distance, Time), not(P \= [ (2, 2, 47), (7, 47, 9)]), not(R \= [ (6, 9, 47), (3, 47, 2)]), Distance is 12.793950362289387, Time is 367.9395036228939.

% Testa lista de id's conhecidos, através do número de ids i.e número de caminhos diferentes - de momento 8.
test(allIDList):- allIDList(ReturnList), length(ReturnList,Size), Size is 8.

:- end_tests(lapr5).

/* The following directive runs the tests. You can also give the goal
   run_tests on the command line (without the :- part). */
:-    run_tests.
